﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows;

namespace WinServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static int _hostPort = 2620;
        IPAddress ip = IPAddress.Parse("25.49.80.120");
        public MainWindow()
        {
            InitializeComponent();
            ip1.Content = ip.ToString();
            port.Content = _hostPort.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var serverCertificate = getServerCert();
            var listener = new TcpListener(ip, _hostPort);
            listener.Start();
     
            this.Dispatcher.Invoke(() =>
            {
                txtresults.Text += "Servidor Iniciado \n";
            });
            while (true)
            {
                using (var client = listener.AcceptTcpClient())
                using (var sslStream = new SslStream(client.GetStream(),
                   false, ValidateCertificate))
                {
                    sslStream.AuthenticateAsServer(serverCertificate,
                       true, SslProtocols.Tls12, false);
                    var inputBuffer = new byte[4096];
                    var inputBytes = 0;
                    while (inputBytes == 0)
                    {
                        inputBytes = sslStream.Read(inputBuffer, 0,
                           inputBuffer.Length);
                    }
                    this.Dispatcher.Invoke(() =>
                    {
                        var inputMessage = Encoding.UTF8.GetString(inputBuffer,
                       0, inputBytes);
                        MessageBox.Show("Se conecto: " + inputMessage);
                        if (inputMessage == "Cliente")
                        {
                            //
                            String nm = Environment.MachineName;
                            int np = Environment.ProcessorCount;
                            String os = Environment.OSVersion.ToString();
                            String a1 = Environment.Version.ToString();
                            Computadora c = new Computadora();
                            c.Name = nm;
                            c.Nprocessor = np;
                            c.osV = os;
                            c.Ver1 = a1;
                            string js = JsonConvert.SerializeObject(c);
                           
                            byte[] data = Encoding.ASCII.GetBytes(js);
                            MessageBox.Show("Mensaje Enviado");
                            var outputBuffer = data;
                            sslStream.Write(outputBuffer);
                        }
                        txtresults.Text += string.Format("Recibido: {0} \n", inputMessage);
                        Console.WriteLine("Conectado con: {0}", inputMessage);
                    });
                }
            }
        }
        static bool ValidateCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //return true;
            if (sslPolicyErrors == SslPolicyErrors.None)
            { return true; }
            if (sslPolicyErrors ==
                  SslPolicyErrors.RemoteCertificateChainErrors)
            { return true; }
            return false;
        }

        private static X509Certificate getServerCert()
        {
            X509Store store = new X509Store(StoreName.My,
               StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            X509Certificate2 foundCertificate = null;
            foreach (X509Certificate2 currentCertificate
               in store.Certificates)
            {
                if (currentCertificate.IssuerName.Name
                   != null && currentCertificate.IssuerName.
                   Name.Equals("CN=sdssl2"))
                {
                    foundCertificate = currentCertificate;
                    break;
                }
            }
            return foundCertificate;
        }
    }
}
